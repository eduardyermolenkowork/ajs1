/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {
    try {
        if(!size || !stuffing){
            throw(HamburgerException(`Hamburger param "size" of "stuffing" is undefined`));
        } else {
            if (size.category === "size") {
                this.getOwnSize = () => size;
            } else {
                throw(HamburgerException(`incorrect param "size"`));
            }
            if (stuffing.category === "stuffing") {
                this.getOwnStuffing = () => stuffing;
            } else {
                throw(HamburgerException(`incorrect param "stuffing"`));
            }
        }
    } catch (err) {
        console.error(`${err}`);
        return {};
    }
    this.ownTopping = [];
}
/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    price: 50,
    calories: 20,
    name: "small",
    category: "size"
};
Hamburger.SIZE_LARGE = {
    price: 100,
    calories: 40,
    name: "large",
    category: "size"
};
Hamburger.STUFFING_CHEESE = {
    price: 10,
    calories: 20,
    name: "cheese",
    category: "stuffing"
};
Hamburger.STUFFING_SALAD = {
    price: 20,
    calories: 5,
    name: "salad",
    category: "stuffing"
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    calories: 10,
    name: "potato",
    category: "stuffing"
};
Hamburger.TOPPING_MAYO = {
    price: 20,
    calories: 5,
    name: "mayo",
    category: "topping"
};
Hamburger.TOPPING_SPICE = {
    price: 15,
    calories: 0,
    name: "spice",
    category: "topping"
};
/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (...topping) {

    /**
    * Функция проверки на дубликаты во входных аргументах
    **/
    function isRepeat (arr) {
        let isRep = false;
        for (let i = 0; i < arr.length; i++) {
            for (let j = 0; j < arr.length; j++) {
                if (j !== i) {
                    isRep = arr[i] === arr[j];
                    if (isRep) {
                        return isRep
                    }
                }
            }
        }
        return isRep;
    }
    /**
    функция проверки - соотвествуют ли входные аргументы категории "добавки"
    **/
    function isTopping(arr) {
        let isTop = true;
        arr.forEach((val)=>{
            if(val.category !== "topping"){
               return isTop = false;
           }
        });
        return isTop;
    }
    /***
    * Валидация аргументов
    *
    ***/
    try {
        if (topping.length === 0) {
            throw HamburgerException(`param "topping" is empty`);
        } else if (topping.length > 2) {
            throw HamburgerException(`to match arguments of param "topping"`)
        } else if (isRepeat(topping)) {
            throw HamburgerException(`repeat of param "topping"`)
        } else if(!isTopping(topping)){
            throw HamburgerException(`param is not category "topping"`)
        }
        else {
            topping.forEach((value)=>{
                if(!this.ownTopping.includes(value)){
                    this.ownTopping.push(value);
                } else throw HamburgerException(`this param already exist of toppings`)
            });

        }
    } catch (e) {
        console.error(`${e}`);
    }
};
/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    try {
        if(this.ownTopping.includes(topping)){
            this.ownTopping.splice(this.ownTopping.indexOf(topping), 1);
        } else {
            throw HamburgerException(`remove param is not exist`)
        }
    } catch (e) {
        console.error(`${e}`);
    }
};
/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */

Hamburger.prototype.getToppings = function () {
    return this.ownTopping.flat(1);
};
/**
 * Узнать размер гамбургера
 */

Hamburger.prototype.getSize = function () {
    return this.getOwnSize().name;
};
/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    let toppingVal = 0;
    if(this.ownTopping.length){
        toppingVal = this.ownTopping
            .map((value) => value["price"])
            .reduceRight(((a, b) => a + b )
            )}
    return this.getOwnSize()["price"] +
        this.getOwnStuffing()["price"] +
        toppingVal;

};
/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function (){
    return this.getOwnStuffing().name;
};
/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    let toppingVal = 0;
    if(this.ownTopping.length){
        toppingVal = this.ownTopping
            .map((value) => value.calories)
            .reduceRight(((a, b) => a + b )
            )}
    return this.getOwnSize().calories +
        this.getOwnStuffing().calories +
        toppingVal;
};
function HamburgerException (message) {
    const err = new Error(message);
    err.name = "HamburgerException";
    return err;
}
/*
============== TESTING ======================
 */
const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(`size: ${hamburger.getSize()}`);
console.log(`stuffing: ${hamburger.getStuffing()}`);
hamburger.addTopping(Hamburger.TOPPING_MAYO, Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
console.log(hamburger.getToppings());
hamburger.getToppings().forEach((value, index)=>{
    console.log(`topping #${index} ${value.name} `);
});
console.log(`price: ${hamburger.calculatePrice()} тугриков`);
console.log(`calories: ${hamburger.calculateCalories()} kCal`);
hamburger.removeTopping(Hamburger.TOPPING_MAYO);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log(hamburger.getToppings().length);
console.log(`price: ${hamburger.calculatePrice()} тугриков`);
const hamburger1 = new Hamburger();



